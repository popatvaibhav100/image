<a href="https://imgbb.com/"><img src="https://i.ibb.co/B3qMQcF/Whats-App-Image-2020-07-17-at-9-53-46-PM.jpg" alt="Whats-App-Image-2020-07-17-at-9-53-46-PM" border="0"></a>

<h3>Chitralab</h3>

## Table of Contents 

- [Installation](#installation)
- [APK](#apk)
- [Features](#features)
- [Team](#team)

## Installation

#### Clone

- Clone this repo to your local machine using 'https://gitlab.com/popatvaibhav100/image.git'

#### Build And Run

-Open the Project Folder in Android studio and Run using emulator or external mobile device.

## APK

The APK file can be downloaded from below link<br/>:
   https://drive.google.com/file/d/1kxVZ30KgXA140yyHOjemFfQd37dquDFv/view?usp=sharing


## Features

<p>
Android app supports following features :
<ul>
<li>Text Recognition from Images</li>
<li>Speech Conversion of Recognized Text</li>
<li>Image Editing (Preset Filters, Color Correction, Emoji, Custom Text, Brush Edit, Add  
  Frames, etc)</li>
<li>Saving and Sharing images.</li>
<li>Capturing and Converting multiple images to generate PDF</li>
</ul>
</p>

## Team

**Members**:
  <ol>
  <li>RAJ BHISE</li>
  <li>VAIBHAV POPAT</li>
  <li>PRATHAMESH MAHANGUDE</li>
  </ol>






---