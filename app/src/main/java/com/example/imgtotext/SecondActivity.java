package com.example.imgtotext;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.media.RatingCompat;
import android.text.Html;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;

public class SecondActivity extends AppCompatActivity {

    private static final int CAMERA_REQUEST_CODE=200;
    private static final int STORAGE_REQUEST_CODE=400;
    private static final int IMAGE_PICK_GALLERY_CODE=1000;
    private static final int IMAGE_PICK_CAMERA_CODE=1001;
    private static final String DATA_PATH = Environment.getExternalStorageDirectory().toString() + "/tesseract_languages/";
    public static final String lang = "eng";
    public boolean dirExist=false;
    public static String Imagedetected="No";
    Context yut;
    String cameraPermission[];
    String storagePermission[];
    ImageView mPreviewIv;
    Uri image_uri;
    ProgressBar pb;
    ProgressBar pb1;
    Uri croppedImageUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        pb=(ProgressBar)findViewById(R.id.pBar);
        pb.setVisibility(View.GONE); //to show

        mPreviewIv = findViewById(R.id.imageIv);

        Button button_upload=(Button)findViewById(R.id.button_upload);
        button_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageImportDialog();
            }
        });

        cameraPermission = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE};
        //storage permission
        storagePermission = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

        Button btn_1=(Button)findViewById(R.id.btn_1);
        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(Imagedetected.equals("Yes"))
                {
                final Intent detecttext = new Intent(getApplicationContext(), ImgToText.class);
                pb = (ProgressBar) findViewById(R.id.pBar);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        pb.setVisibility(View.VISIBLE);
                    }
                });
                AsyncTask.execute(
                        new Runnable() {

                            @Override
                            public void run() {
                                System.out.println("Inside Run..............");
                                BitmapDrawable bitmapDrawable = (BitmapDrawable) mPreviewIv.getDrawable();
                                Bitmap bitmap = bitmapDrawable.getBitmap();
                                TextRecognizer recognizer = new TextRecognizer.Builder(getApplicationContext()).build();
                                Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                                SparseArray<TextBlock> items = recognizer.detect(frame);
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < items.size(); i++) {
                                    TextBlock myItem = items.valueAt(i);
                                    sb.append(myItem.getValue());
                                    sb.append("\n");
                                }
                                final String recognizedText = sb.toString();

                                System.out.println("Detected Text..............." + recognizedText);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pb.setVisibility(View.GONE); //to show
                                        detecttext.putExtra("Recognised", recognizedText);
                                        //detecttext.putExtra("Image", byteArray);
                                        detecttext.putExtra("Image", croppedImageUri.toString());
                                        Log.i("TAG", croppedImageUri.toString());
                                        if (mPreviewIv.getDrawable() != null) {
                                            startActivity(detecttext);
                                        }
                                    }
                                });
                            }
                        }
                );
            }
             else
            {
                Toast toast=Toast.makeText(getApplicationContext(),"Please upload the image first",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

            }

            }
        });

        Button btn_2=(Button)findViewById(R.id.btn_2);
        btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Imagedetected.equals("Yes")) {

                    pb1=(ProgressBar)findViewById(R.id.pBar);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pb.setVisibility(View.VISIBLE); //to show
                        }
                    });


                    AsyncTask.execute(
                            new Runnable() {

                                @Override
                                public void run() {

                                    final Intent editimage = new Intent(getApplicationContext(), ImgEditor.class);
                                    BitmapDrawable bitmapDrawable = (BitmapDrawable) mPreviewIv.getDrawable();
                                    Bitmap bitmap = bitmapDrawable.getBitmap();
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                    final byte[] byteArray = stream.toByteArray();
                                    editimage.putExtra("Image", croppedImageUri.toString());
                                    Log.i("TAG", croppedImageUri.toString());
                                    if (mPreviewIv.getDrawable() != null) {
                                        startActivity(editimage);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pb.setVisibility(View.GONE); //to show
                                            }
                                        });
                                    }
                                }
                            });
                }
                else
                {

                    Toast toast=Toast.makeText(getApplicationContext(),"Please upload the image first",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                }
            }
        });

        Button btn_3=findViewById(R.id.btn_3);
        btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent makepdf=new Intent(getApplicationContext(),ImgToPdf.class);
//                startActivity(makepdf);

                Intent makegrid=new Intent(getApplicationContext(),gridactivity.class);
                startActivity(makegrid);


            }
        });

    }


    private void showImageImportDialog() {
        //Items to display in dialog
        String[] items = {" Camera"," Gallery"};
        AlertDialog.Builder dialog=new AlertDialog.Builder(this,R.style.MyDialogTheme);
        //Set Title
        dialog.setTitle("Select Image");
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which==0){
                    //Camera option Clicked
                    if(!checkCameraPermission()){
                        requestCameraPermission();
                    }
                    else{
                        pickCamera();
                    }
                }
                if(which==1){
                    //Gallery option Clicked
                    if(!checkStoragePermission()){
                        requestStoragePermission();
                    }
                    else{
                        pickGallery();
                    }
                }
            }
        });
        dialog.create().show();
    }

    private void pickGallery() {
        //intent to pick image from gallery
        Intent intent = new Intent(Intent.ACTION_PICK);
        //set intent type to image
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_GALLERY_CODE);
    }

    private void pickCamera() {
        //Intent to take image from camera and saving to storage to get high quality image
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE,"NewPic");
        values.put(MediaStore.Images.Media.DESCRIPTION,"Image To Text");
        image_uri =getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,values);
        System.out.println("Image_Uri:"+image_uri);


        Intent cameraIntent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,image_uri);
        startActivityForResult(cameraIntent,IMAGE_PICK_CAMERA_CODE);
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this,storagePermission,STORAGE_REQUEST_CODE);
    }

    private boolean checkStoragePermission() {
        boolean result= ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)==(PackageManager.PERMISSION_GRANTED);
        return result;
    }

    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this,cameraPermission,CAMERA_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        boolean result= ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)==(PackageManager.PERMISSION_GRANTED);
        boolean result1= ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)==(PackageManager.PERMISSION_GRANTED);
        return result && result1;
    }

    //handle permission result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case CAMERA_REQUEST_CODE:
                if(grantResults.length>0){
                    boolean cameraAccepted = grantResults[0]==PackageManager.PERMISSION_GRANTED;
                    boolean writeStorageAccepted = grantResults[0]==PackageManager.PERMISSION_GRANTED;
                    if(cameraAccepted && writeStorageAccepted){
                        pickCamera();
                    }
                    else{
                        Toast.makeText(this,"Permission Denied",Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case STORAGE_REQUEST_CODE:
                if(grantResults.length>0){
                    boolean writeStorageAccepted = grantResults[0]==PackageManager.PERMISSION_GRANTED;
                    if(writeStorageAccepted){
                        pickGallery();
                    }
                    else{
                        Toast.makeText(this,"Permission Denied",Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    //handle image result

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        //got image from camera
//        i = pgsBar.getProgress();
//        new Thread(new Runnable() {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("Here..........");
        if (resultCode == RESULT_OK) {
            System.out.println("Here1..........");
            if (requestCode == IMAGE_PICK_GALLERY_CODE) {
                CropImage.activity(data.getData()).setGuidelines(CropImageView.Guidelines.ON).start(this);
            }
            if (requestCode == IMAGE_PICK_CAMERA_CODE) {
                CropImage.activity(image_uri).setGuidelines(CropImageView.Guidelines.ON).start(this);
            }
        }
        //get cropped image
        System.out.println("THere..........");
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            System.out.println("Here2..........");
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
//                    new Thread(new Runnable() {
                System.out.println("Starting...");
                Uri resultUri = result.getUri();
                croppedImageUri=resultUri;
                mPreviewIv.setImageURI(resultUri);
                final Context yut=this;
                Imagedetected="Yes";
            }
            else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                Exception error = result.getError();
                Toast.makeText(this,""+error, Toast.LENGTH_SHORT).show();
            }
        }

    }

}