package com.example.imgtotext;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.example.imgtotext.Adapter.ViewPagerAdapter;
import com.example.imgtotext.Interface.AddFrameListener;
import com.example.imgtotext.Interface.AddTextFragmentListener;
import com.example.imgtotext.Interface.BrushFragmentListener;
import com.example.imgtotext.Interface.EditImageFragmentListener;
import com.example.imgtotext.Interface.EmojiFragmentListener;
import com.example.imgtotext.Interface.FiltersListFragmentListener;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.ContrastSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.SaturationSubfilter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import ja.burhanrashid52.photoeditor.OnSaveBitmap;
import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;

import static androidx.fragment.app.FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class ImgEditor extends AppCompatActivity implements FiltersListFragmentListener, EditImageFragmentListener, BrushFragmentListener, EmojiFragmentListener, AddTextFragmentListener, AddFrameListener {
    public static Bitmap bmp;
    private static final int STORAGE_REQUEST_CODE=400;
    PhotoEditorView img_preview;
    PhotoEditor photoEditor;
    Uri image_uri;
    CoordinatorLayout coordinatorLayout;
    String storagePermission[];
    Bitmap originalBitmap,filteredBitmap,finalBitmap;

    FiltersListFragment filtersListFragment;
    EditImageFragment editImageFragment;

    CardView btn_filters_list,btn_edit,btn_brush,btn_emoji,btn_add_text,btn_add_frame;

    int brightnessFinal=0;
    float contrastFinal=1.0f;
    float saturationFinal=1.0f;

    static{
        System.loadLibrary("NativeImageProcessor");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_editor);

        Intent intent =getIntent();
        if(intent.hasExtra("Image")){
            String resultUri=intent.getExtras().getString("Image");
            Uri FinalImage=Uri.parse(resultUri);
            img_preview = (PhotoEditorView) findViewById(R.id.edit_image_preview);
            Log.i("TAG", "Result is:"+resultUri);
            ImageView image = (ImageView) img_preview.getSource();
            image.setImageURI(FinalImage);
            photoEditor = new PhotoEditor.Builder(this,img_preview)
                    .setPinchTextScalable(true)
                    .setDefaultEmojiTypeface(Typeface.createFromAsset(getAssets(),"NotoColorEmoji.ttf"))
                    .build();
        }
        BitmapDrawable bitmapDrawable=(BitmapDrawable)img_preview.getSource().getDrawable();
        bmp=bitmapDrawable.getBitmap();

        btn_filters_list = (CardView)findViewById(R.id.btn_filters_list);
        btn_edit = (CardView)findViewById(R.id.btn_edit_list);
        btn_brush = (CardView)findViewById(R.id.btn_brush);
        btn_emoji = (CardView)findViewById(R.id.btn_emoji);
        btn_add_text = (CardView)findViewById(R.id.btn_add_text);
        btn_add_frame = (CardView)findViewById(R.id.btn_add_frame);

        btn_filters_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FiltersListFragment filtersListFragment = FiltersListFragment.getInstance();
                filtersListFragment.setListener(ImgEditor.this);
                filtersListFragment.show(getSupportFragmentManager(),filtersListFragment.getTag());
            }
        });
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditImageFragment editImageFragment = EditImageFragment.getInstance();
                editImageFragment.setListener(ImgEditor.this);
                editImageFragment.show(getSupportFragmentManager(),editImageFragment.getTag());
            }
        });
        btn_brush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoEditor.setBrushDrawingMode(true);
                BrushFragment brushFragment = BrushFragment.getInstance();
                brushFragment.setListener(ImgEditor.this);
                brushFragment.show(getSupportFragmentManager(),brushFragment.getTag());
            }
        });
        btn_emoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmojiFragment emojiFragment = EmojiFragment.getInstance();
                emojiFragment.setListener(ImgEditor.this);
                emojiFragment.show(getSupportFragmentManager(),emojiFragment.getTag());
            }
        });
        btn_add_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTextFragment addTextFragment =AddTextFragment.getInstance();
                addTextFragment.setListener(ImgEditor.this);
                addTextFragment.show(getSupportFragmentManager(),addTextFragment.getTag());
            }
        });
        btn_add_frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FrameFragment frameFragment = FrameFragment.getInstance();
                frameFragment.setListener(ImgEditor.this);
                frameFragment.show(getSupportFragmentManager(),frameFragment.getTag());
            }
        });
        loadImage();
    }

    private void loadImage() {
        originalBitmap=bmp;
        filteredBitmap=originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
        finalBitmap=originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
        img_preview.getSource().setImageBitmap(originalBitmap);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter= new ViewPagerAdapter(getSupportFragmentManager(),BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        filtersListFragment = new FiltersListFragment();
        filtersListFragment.setListener(this);

        editImageFragment = new EditImageFragment();
        editImageFragment.setListener(this);
        adapter.addFragment(filtersListFragment,"FILTERS");
        adapter.addFragment(editImageFragment,"EDIT");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onBrightnessChanged(int brightness) {
        brightnessFinal=brightness;
        Filter myFilter= new Filter();
        myFilter.addSubFilter(new BrightnessSubFilter(brightness));
        img_preview.getSource().setImageBitmap(myFilter.processFilter(finalBitmap.copy(Bitmap.Config.ARGB_8888,true)));
    }

    @Override
    public void onContrastChanged(float contrast) {
        contrastFinal=contrast;
        Filter myFilter= new Filter();
        myFilter.addSubFilter(new ContrastSubFilter(contrast));
        img_preview.getSource().setImageBitmap(myFilter.processFilter(finalBitmap.copy(Bitmap.Config.ARGB_8888,true)));
    }

    @Override
    public void onSaturationChanged(float saturation) {
        saturationFinal=saturation;
        Filter myFilter= new Filter();
        myFilter.addSubFilter(new SaturationSubfilter(saturation));
        img_preview.getSource().setImageBitmap(myFilter.processFilter(finalBitmap.copy(Bitmap.Config.ARGB_8888,true)));
    }

    @Override
    public void onEditStarted() {

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onEditCompleted() {
        Bitmap bitmap = filteredBitmap.copy(Bitmap.Config.ARGB_8888,true);
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new BrightnessSubFilter(brightnessFinal));
        myFilter.addSubFilter(new ContrastSubFilter(contrastFinal));
        myFilter.addSubFilter(new SaturationSubfilter(saturationFinal));
        finalBitmap=myFilter.processFilter(bitmap);
    }

    @Override
    public void onFilterSelected(Filter filter) {
        resetControl();
        filteredBitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
        img_preview.getSource().setImageBitmap(filter.processFilter(filteredBitmap));
        finalBitmap=filteredBitmap.copy(Bitmap.Config.ARGB_8888,true);

    }

    private void resetControl() {
        if(editImageFragment!=null){
            editImageFragment.resetControls();
            brightnessFinal=0;
            contrastFinal=1.0f;
            saturationFinal=1.0f;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_image,menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.action_save){
            saveImageToGallery();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this,storagePermission,STORAGE_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void saveImageToGallery() {
        if(!checkStoragePermission()){
            requestStoragePermission();
        }

        photoEditor.saveAsBitmap(new OnSaveBitmap() {
            @Override
            public void onBitmapReady(Bitmap saveBitmap) {
                ImageView iv = img_preview.getSource();
                iv.setImageBitmap(saveBitmap);
                FileOutputStream outStream = null;
                File dir = new File(Environment.getExternalStorageDirectory() , "/Img_World");
                boolean resva = dir.mkdirs();
                long name = System.currentTimeMillis();
                String fileName = String.format("%d.jpg", name);
                File outFile = new File(dir, fileName);
                try {
                    outStream = new FileOutputStream(outFile);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap bitmap = ((BitmapDrawable) iv.getDrawable()).getBitmap();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                try {
                    outStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                final String photoPath = Environment.getExternalStorageDirectory()+ "/Img_World/"+name+".jpg";
                AlertDialog.Builder dialog=new AlertDialog.Builder(ImgEditor.this,R.style.MyDialogTheme1);


                String s1="<b>"+"Done!"+"</b>";
                String s2="<font color='#696969'>File saved at: </font>";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    dialog.setMessage(Html.fromHtml(s1,Html.FROM_HTML_MODE_LEGACY)+"\n"+"\n"+Html.fromHtml(s2,Html.FROM_HTML_MODE_LEGACY));
                } else {
                    dialog.setMessage(Html.fromHtml(s1)+"\n"+"\n"+Html.fromHtml(s2)+photoPath);
                }


                dialog.setPositiveButton(Html.fromHtml("CLOSE"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                //Button Two : Open
                dialog.setNegativeButton(Html.fromHtml("OPEN"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse("file://"+photoPath), "image/*");
                        startActivity(intent);
                    }
                });

                //Button Three : Neutral
                dialog.setNeutralButton(Html.fromHtml("SHARE"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        File newfile=new File(photoPath);
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                shareIntent.setType("image/*");
                Uri uri = Uri.fromFile(newfile);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                startActivity(Intent.createChooser(shareIntent,"Share Via"));
                    }
                });


                dialog.create().show();


            }

            @Override
            public void onFailure(Exception e) {

            }
        });

    }

    private boolean checkStoragePermission() {
        boolean result= ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)==(PackageManager.PERMISSION_GRANTED);
        return result;
    }

    @Override
    public void onBrushSizeChangedListener(float size) {
        photoEditor.setBrushSize(size);
    }

    @Override
    public void onBrushOpacityChangedListener(int opacity) {
        photoEditor.setOpacity(opacity);
    }

    @Override
    public void onBrushColorChangedListener(int color) {
        photoEditor.setBrushColor(color);
    }

    @Override
    public void onBrushStateChangedListener(boolean isEraser) {
        if(isEraser){
            photoEditor.brushEraser();
        }
        else{
            photoEditor.setBrushDrawingMode(true);
        }
    }

    @Override
    public void onEmojiSelected(String emoji) {
        photoEditor.addEmoji(emoji);
    }


    @Override
    public void onAddTextButtonClick(Typeface typeface, String text, int color) {
        photoEditor.addText(typeface,text,color);
    }

    @Override
    public void onAddFrame(int frame) {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),frame);
        photoEditor.addImage(bitmap);
    }
}