package com.example.imgtotext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;

import java.util.Locale;

public class ImgToText extends AppCompatActivity {

    TextToSpeech mtts;
    SeekBar mSeekBarPitch;
    SeekBar mSeekBarSpeed;
    Button mButtonSpeak;
    Button mButtonStop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_to_text);

        mSeekBarPitch = findViewById(R.id.seek_bar_pitch);
        mSeekBarSpeed = findViewById(R.id.seek_bar_speed);
        mButtonSpeak = findViewById(R.id.button_speak);
        mButtonStop = findViewById(R.id.button_stop);

        mtts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status==TextToSpeech.SUCCESS){
                    int result = mtts.setLanguage(Locale.ENGLISH);
                    if(result==TextToSpeech.LANG_MISSING_DATA||result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("TTS","Language not supported");
                    }
                    else{
                        mButtonSpeak.setEnabled(true);
                        mButtonStop.setEnabled(true);
                    }
                }
                else{
                    Log.e("TTS","Initialization Failed");
                }
            }
        });
        mButtonSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speak();
            }
        });
        mButtonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop();
            }
        });
        Intent intent=getIntent();
        if(intent.hasExtra("Recognised"))
        {
            String text=intent.getExtras().getString("Recognised");
            EditText resultEt = (EditText)findViewById(R.id.resultEt);
            resultEt.setText(text);
        }
        if(intent.hasExtra("Image"))
        {
            String resultUri=intent.getExtras().getString("Image");
            Uri FinalImage=Uri.parse(resultUri);
            ImageView image = (ImageView) findViewById(R.id.imagepreview);
            image.setImageURI(FinalImage);
        }

    }
    private void speak(){
        EditText resultEt = (EditText)findViewById(R.id.resultEt);
        String text=resultEt.getText().toString();
        float pitch = (float) mSeekBarPitch.getProgress()/50;
        if(pitch<0.1) pitch=0.1f;
        float speed = (float) mSeekBarSpeed.getProgress()/50;
        if(speed<0.1) speed=0.1f;
        mtts.setPitch(pitch);
        mtts.setSpeechRate(speed);
        mtts.speak(text,TextToSpeech.QUEUE_FLUSH,null);
    }
    private void stop(){
        EditText resultEt = (EditText)findViewById(R.id.resultEt);
        String text=resultEt.getText().toString();
        mtts.stop();
    }
    @Override
    protected void onDestroy() {
        if(mtts!=null){
            mtts.stop();
            mtts.shutdown();
        }
        super.onDestroy();
    }
}