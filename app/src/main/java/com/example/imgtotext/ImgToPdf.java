package com.example.imgtotext;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.widget.GridLayout;


import android.Manifest;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ImgToPdf extends AppCompatActivity {

    private static final int CAMERA_REQUEST_CODE=200;
    private static final int STORAGE_REQUEST_CODE=400;
    private static final int IMAGE_PICK_GALLERY_CODE=1000;
    private static final int IMAGE_PICK_CAMERA_CODE=1001;

    int PICK_IMAGE_MULTIPLE = 1;
    String imageEncoded;
    List<String> imagesEncodedList;

    String cameraPermission[];
    String storagePermission[];
    Uri image_uri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_to_pdf2);


        Button button_upload=(Button)findViewById(R.id.button_upload);
        button_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageImportDialog();
            }
        });

//        final GridLayout layout = (GridLayout) findViewById(R.id.gridlayout);
//        ViewTreeObserver vto = layout.getViewTreeObserver();
//        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
//                    layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//                } else {
//                    layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                }
//                int width = layout.getMeasuredWidth();
//                int height = layout.getMeasuredHeight();
//                setViews(height, width);
//
//


        }



//    private void setViews(int layoutHeight, int layoutWidth) {
//
//        int width = layoutWidth / 3;
//        int height = layoutHeight / 2;
//        Log.i(getClass().getName(), "Image height" + height + " Width:" + width);
//        ArrayList<String> data = new ArrayList<>();
//        data.clear();
//        GridLayout gridLayout = (GridLayout) findViewById(R.id.gridlayout);
//        gridLayout.removeAllViews();
//        Random random = new Random();
//        int num = 3;
//        for (int i = 0; i < num; i++) {
//            data.add(String.format("%." + 0 + "f", random.nextDouble() * (6 - 1) + 1));
//            ImageView imageView = new ImageView(this);
//            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
//            layoutParams.width = width;
//            layoutParams.height = height;
//            imageView.setLayoutParams(layoutParams);
//            switch (data.get(i)) {
//                case "1":
//                    imageView.setImageResource(R.drawable.edit_image);
//                    break;
//                case "2":
//                    imageView.setImageResource(R.drawable.recognize_text);
//                    break;
//                default:
//                    imageView.setImageResource(R.drawable.edit_image);
//                    break;
//            }
//            gridLayout.addView(imageView, i);
//        }
//    }


    private void showImageImportDialog() {
        //Items to display in dialog
        String[] items = {" Camera"," Gallery"};
        AlertDialog.Builder dialog=new AlertDialog.Builder(this);
        //Set Title
        dialog.setTitle("Select Image");
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which==0){
                    //Camera option Clicked
                    if(!checkCameraPermission()){
                        requestCameraPermission();
                    }
                    else{
                        pickCamera();
                    }
                }
                if(which==1){
                    //Gallery option Clicked
                    if(!checkStoragePermission()){
                        requestStoragePermission();
                    }
                    else{
                        pickGallery();
                    }
                }
            }
        });
        dialog.create().show();
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this,storagePermission,STORAGE_REQUEST_CODE);
    }

    private boolean checkStoragePermission() {
        boolean result= ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)==(PackageManager.PERMISSION_GRANTED);
        return result;
    }

    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this,cameraPermission,CAMERA_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        boolean result= ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)==(PackageManager.PERMISSION_GRANTED);
        boolean result1= ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)==(PackageManager.PERMISSION_GRANTED);
        return result && result1;
    }

    private void pickGallery() {
        //intent to pick image from gallery
        Intent intent = new Intent(Intent.ACTION_PICK);
        //set intent type to image
        intent.setType("image/*");
//        startActivityForResult(intent, IMAGE_PICK_GALLERY_CODE);

        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_MULTIPLE);

    }

    private void pickCamera() {
        //Intent to take image from camera and saving to storage to get high quality image
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE,"NewPic");
        values.put(MediaStore.Images.Media.DESCRIPTION,"Image To Text");
        image_uri =getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,values);
        System.out.println("Image_Uri:"+image_uri);


        Intent cameraIntent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,image_uri);
        startActivityForResult(cameraIntent,IMAGE_PICK_CAMERA_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {


        try {
            // When an Image is picked
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                imagesEncodedList = new ArrayList<String>();
                if(data.getData()!=null){

                    Uri mImageUri=data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded  = cursor.getString(columnIndex);
                    cursor.close();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded  = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();

                        }

                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }
                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

        super.onActivityResult(requestCode, resultCode, data);

//        super.onActivityResult(requestCode, resultCode, data);
//
//        System.out.println("AAYYA CHO.....");
//
//        final ImageView iv=findViewById(R.id.imageIv);
//        Uri resultUri = data.getData();
//        iv.setImageURI(resultUri);
//
//        Button btn_2=(Button)findViewById(R.id.btn_2);
//        btn_2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Document document = new Document();
//
//                FileOutputStream outStream = null;
//                File dir = new File(Environment.getExternalStorageDirectory() , "/Img_World");
//                boolean resva = dir.mkdirs();
//                long name = System.currentTimeMillis();
//                String fileName = String.format("Pdf"+"%d.jpg", name);
//                File outFile = new File(dir, fileName);
//
//                try {
//                    outStream = new FileOutputStream(outFile);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//                String directoryPath = android.os.Environment.getExternalStorageDirectory().toString();
//
//                try {
//                    try {
//                        PdfWriter.getInstance(document, new FileOutputStream(dir + "/example.pdf")); //  Change pdf's name.
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    }
//                } catch (DocumentException e) {
//                    e.printStackTrace();
//                }
//
//                Bitmap bitmap = ((BitmapDrawable) iv.getDrawable()).getBitmap();
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
//
//                document.open();
//
//                Image image = null;  // Change image's name and extension.
//                try {
//                    image = Image.getInstance(Environment.getExternalStorageDirectory() + "/Img_World/"+"Pdf"+name+".jpg");
//                } catch (BadElementException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//
//                float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
//                        - document.rightMargin() - 0) / image.getWidth()) * 100; // 0 means you have no indentation. If you have any, change it.
//                image.scalePercent(scaler);
//                image.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
//
//                try {
//                    document.add(image);
//                } catch (DocumentException e) {
//                    e.printStackTrace();
//                }
//                document.close();
//                try {
//                    outStream.flush();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    outStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        });

    }


}