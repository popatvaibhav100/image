package com.example.imgtotext.Adapter;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.imgtotext.R;

import java.util.ArrayList;
import java.util.List;

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.ColorViewholder> {
    Context context;
    List<Integer> colorList;
    ColorAdapterListener listener;

    public ColorAdapter(Context context, ColorAdapterListener listener) {
        this.context = context;
        this.colorList = genColorList();
        this.listener = listener;
    }

    @NonNull
    @Override
    public ColorAdapter.ColorViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.color_item,parent,false);
        return new ColorViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorAdapter.ColorViewholder holder, int position) {
        holder.color_section.setBackgroundColor(colorList.get(position));
    }

    @Override
    public int getItemCount() {
        return colorList.size();
    }

    public class ColorViewholder extends RecyclerView.ViewHolder{

        public CardView color_section;
        public ColorViewholder(@NonNull final View itemView) {
            super(itemView);
            color_section=(CardView)itemView.findViewById(R.id.color_section);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onColorSelected(colorList.get(getAdapterPosition()));
                    Drawable earlier = color_section.getBackground();
                    Drawable highlight = v.getResources().getDrawable(R.drawable.highlight);
                    color_section.setBackground(highlight);
                }
            });
        }
    }
    private List<Integer> genColorList(){
        List<Integer>colorList = new ArrayList<>();
        colorList.add(Color.parseColor("#CD5C5C"));
        colorList.add(Color.parseColor("#F08080"));
        colorList.add(Color.parseColor("#FA8072"));
        colorList.add(Color.parseColor("#E9967A"));
        colorList.add(Color.parseColor("#FFA07A"));
        colorList.add(Color.parseColor("#DC143C"));
        colorList.add(Color.parseColor("#FF0000"));
        colorList.add(Color.parseColor("#B22222"));
        colorList.add(Color.parseColor("#8B0000"));
        colorList.add(Color.parseColor("#FFC0CB"));
        colorList.add(Color.parseColor("#FFB6C1"));
        colorList.add(Color.parseColor("#FF69B4"));
        colorList.add(Color.parseColor("#FF1493"));
        colorList.add(Color.parseColor("#C71585"));
        colorList.add(Color.parseColor("#DB7093"));
        colorList.add(Color.parseColor("#FFA07A"));
        colorList.add(Color.parseColor("#FF7F50"));
        colorList.add(Color.parseColor("#FF6347"));
        colorList.add(Color.parseColor("#FF4500"));
        colorList.add(Color.parseColor("#FF8C00"));
        colorList.add(Color.parseColor("#FFA500"));
        colorList.add(Color.parseColor("#FFD700"));
        colorList.add(Color.parseColor("#FFFF00"));
        colorList.add(Color.parseColor("#FFFFE0"));
        colorList.add(Color.parseColor("#FFFACD"));
        colorList.add(Color.parseColor("#FAFAD2"));
        colorList.add(Color.parseColor("#FFEFD5"));
        colorList.add(Color.parseColor("#FFE4B5"));
        colorList.add(Color.parseColor("#FFDAB9"));
        colorList.add(Color.parseColor("#EEE8AA"));
        colorList.add(Color.parseColor("#F0E68C"));
        colorList.add(Color.parseColor("#BDB76B"));
        colorList.add(Color.parseColor("#E6E6FA"));
        colorList.add(Color.parseColor("#D8BFD8"));
        colorList.add(Color.parseColor("#DDA0DD"));
        colorList.add(Color.parseColor("#EE82EE"));
        colorList.add(Color.parseColor("#DA70D6"));
        colorList.add(Color.parseColor("#FF00FF"));
        colorList.add(Color.parseColor("#BA55D3"));
        colorList.add(Color.parseColor("#9370DB"));
        colorList.add(Color.parseColor("#663399"));
        colorList.add(Color.parseColor("#8A2BE2"));
        colorList.add(Color.parseColor("#9400D3"));
        colorList.add(Color.parseColor("#800080"));
        colorList.add(Color.parseColor("#4B0082"));
        colorList.add(Color.parseColor("#6A5ACD"));
        colorList.add(Color.parseColor("#483D8B"));
        colorList.add(Color.parseColor("#7B68EE"));
        colorList.add(Color.parseColor("#ADFF2F"));
        colorList.add(Color.parseColor("#7FFF00"));
        colorList.add(Color.parseColor("#7CFC00"));
        colorList.add(Color.parseColor("#00FF00"));
        colorList.add(Color.parseColor("#32CD32"));
        colorList.add(Color.parseColor("#98FB98"));
        colorList.add(Color.parseColor("#90EE90"));
        colorList.add(Color.parseColor("#00FA9A"));
        colorList.add(Color.parseColor("#00FF7F"));
        colorList.add(Color.parseColor("#3CB371"));
        colorList.add(Color.parseColor("#2E8B57"));
        colorList.add(Color.parseColor("#228B22"));
        colorList.add(Color.parseColor("#008000"));
        colorList.add(Color.parseColor("#006400"));
        colorList.add(Color.parseColor("#9ACD32"));
        colorList.add(Color.parseColor("#6B8E23"));
        colorList.add(Color.parseColor("#66CDAA"));
        colorList.add(Color.parseColor("#8FBC8B"));
        colorList.add(Color.parseColor("#20B2AA"));
        colorList.add(Color.parseColor("#008080"));
        colorList.add(Color.parseColor("#00FFFF"));
        colorList.add(Color.parseColor("#AFEEEE"));
        colorList.add(Color.parseColor("#7FFFD4"));
        colorList.add(Color.parseColor("#40E0D0"));
        colorList.add(Color.parseColor("#00CED1"));
        colorList.add(Color.parseColor("#5F9EA0"));
        colorList.add(Color.parseColor("#4682B4"));
        colorList.add(Color.parseColor("#B0C4DE"));
        colorList.add(Color.parseColor("#ADD8E6"));
        colorList.add(Color.parseColor("#87CEEB"));
        colorList.add(Color.parseColor("#87CEFA"));
        colorList.add(Color.parseColor("#00BFFF"));
        colorList.add(Color.parseColor("#1E90FF"));
        colorList.add(Color.parseColor("#6495ED"));
        colorList.add(Color.parseColor("#7B68EE"));
        colorList.add(Color.parseColor("#4169E1"));
        colorList.add(Color.parseColor("#0000FF"));
        colorList.add(Color.parseColor("#0000CD"));
        colorList.add(Color.parseColor("#00008B"));
        colorList.add(Color.parseColor("#191970"));
        colorList.add(Color.parseColor("#FFF8DC"));
        colorList.add(Color.parseColor("#FFE4C4"));
        colorList.add(Color.parseColor("#F5DEB3"));
        colorList.add(Color.parseColor("#DEB887"));
        colorList.add(Color.parseColor("#BC8F8F"));
        colorList.add(Color.parseColor("#F4A460"));
        colorList.add(Color.parseColor("#DAA520"));
        colorList.add(Color.parseColor("#B8860B"));
        colorList.add(Color.parseColor("#CD853F"));
        colorList.add(Color.parseColor("#D2691E"));
        colorList.add(Color.parseColor("#8B4513"));
        colorList.add(Color.parseColor("#A0522D"));
        colorList.add(Color.parseColor("#A52A2A"));
        colorList.add(Color.parseColor("#800000"));
        colorList.add(Color.parseColor("#FFFFFF"));
        colorList.add(Color.parseColor("#FFFAFA"));
        colorList.add(Color.parseColor("#F0FFF0"));
        colorList.add(Color.parseColor("#F5F5F5"));
        colorList.add(Color.parseColor("#FFFAF0"));
        colorList.add(Color.parseColor("#DCDCDC"));
        colorList.add(Color.parseColor("#D3D3D3"));
        colorList.add(Color.parseColor("#C0C0C0"));
        colorList.add(Color.parseColor("#808080"));
        colorList.add(Color.parseColor("#696969"));
        colorList.add(Color.parseColor("#778899"));
        colorList.add(Color.parseColor("#708090"));
        colorList.add(Color.parseColor("#2F4F4F"));
        colorList.add(Color.parseColor("#000000"));
        return colorList;
    }
    public interface ColorAdapterListener{
        void onColorSelected(int color);
    }
}
