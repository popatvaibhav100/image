package com.example.imgtotext.Interface;


import com.zomato.photofilters.imageprocessors.Filter;

public interface FiltersListFragmentListener {
    void onFilterSelected(Filter filter);
}
