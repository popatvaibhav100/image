package com.example.imgtotext.Interface;

public interface EmojiFragmentListener {
    void onEmojiSelected(String emoji);
}
