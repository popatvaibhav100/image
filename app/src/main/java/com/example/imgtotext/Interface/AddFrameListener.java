package com.example.imgtotext.Interface;

public interface AddFrameListener {
    void onAddFrame(int frame);
}
